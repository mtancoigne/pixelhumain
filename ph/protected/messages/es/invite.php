<?php

	return array(
                "Search or invite your contacts" => "Busca o invita a tus contactos",
                "Search or invite members" => "Buscar o invitar a miembros",
                "Import a file" => "Importar un archivo",
                "List of persons invited" => "Lista de personas invitadas",
                "Add to the list" => "Añadir a la lista",
                "Files (CSV)" => "Archivos (CSV)",
                "Custom message" => "Mensaje personalizado", 
                "You must use a CSV format" => "Debes utilizar un formato CSV",
                "We were unable to read your file." => "No hemos podido leer tu archivo",
                "Already in the list" => "Ya está en la lista",
                "My contacts" => "Mis contactos",
                "This user is already in your contacts" => "Este usuario ya está en tus contactos.",
                "A name, an e-mail..." => "Un nombre, un correo electrónico ...",
                "Please validate the current invites" => "Por favor valida las invitaciones actuales",
                "This person is already on your contacts" => "Esta persona ya está en tus contactos.",
                "Write" => "Ecribir",
                "Others" => "Otros",
                "Check" => "Verificar",
	);

?>