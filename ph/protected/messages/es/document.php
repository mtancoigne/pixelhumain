<?php

	return array(
		"only" => "Solo",
		"Document has been deleted" => "El documento ha sido eliminado",
		"files are allowed!" => "los archivos están disponibles",
		"Error! Wrong HTTP method!" => "¡Error! Método HTTP incorrecto",
		"Something went wrong with your upload!" => "¡Algo salió mal con tu carga!",
		"Document deleted" => "Documento eliminado",
		"Document saved successfully" => "Documento guardado correctamente",
		"Image deleted" => "Imagen eliminada",
		"You have no rights upload document on this item, just write a message !" => "No tienes derecho para subir este documento en este tema. ¡Escribe un mensaje!",
		"You are not allowed to modify the document of this item !" => "¡No tienes permiso para modificar el documento de este tema!",
		"Collection of bookmarks<br>To classified favorite urls<br>And share it to the community"=>"Colección de marcadores <br>Para clasificar favoritos <br>Y compartirlo con la comunidad",
		"Collection of files<br>To download and share documents<br>With the community"=>"Colección de archivos<br>Para descargar y compartir documentos<br>Con la comunidad",		
	)

?>