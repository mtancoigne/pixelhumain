<?php

return array(

	//GLOBAL INTERFACE 

	"Rooms"=>"Espaces CO",

	"Resolutions"=>"Résolutions",
	"Actions"=>"Actions",
	"Proposals"=>"Propositions",
	"Rooms"=>"Espaces CO",

	"resolution"=>"Résolutions",
	"action"=>"Actions",
	"proposal"=>"Propositions",
	"CO-space"=>"Espaces CO",

	"Create action" => "Créer une action",
	"Add action" => "Ajouter une action",
	"My actions" => "Mes actions",
	"To do" => "À faire",
	"Done" => "Terminées",
	"Archived" => "Archivées",
	"Disabled" => "Désactivées",
	"Resolved" => "Résolues",

	"Create proposal" => "Créer une proposition",
	"Add proposal" => "Ajouter une proposition",
	"My proposals" => "Mes propositions",
	"Amendables" => "Amendables",
	"Amendable" => "Amendable",
	"To vote" => "À voter",
	"Closed" => "Fermées",

	"mine" => "Dont je suis l'auteur",
	"Author" => "Auteur",
	"amendables" => "Amendables",
	"amendable" => "Amendable",
	"tovote" => "À voter",
	"amendementAndVote" => "À voter et à amender",
	"todo" => "À faire",
	"archived" => "Archivée",
	"disabled" => "Désactivée",
	"resolved" => "Résolue",
	"closed" => "Fermée",
	"done" => "Terminée",

	"late" => "En retard",
	"progress" => "En cours",
	"nodate" => "Date indéfinie",
	"startingsoon" => "Commence bientôt",

	"Create room" => "Créer un nouvel espace",
	
	"Topic" => "Sujet",

	"No proposal" => "Aucune proposition",
	"No action" => "Aucune action",
	"No resolution" => "Aucune résolution",

	"end" => "fin",
	"Search in actions" => "Recherche actions",
	"Search in proposals" => "Recherche propositions",
	"Search in resolution" => "Recherche résolutions",
	
	"up" => "Pour",
	"down" => "Contre",
	"white" => "Blanc",
	"uncomplet" => "Incomplet",

	"Agree" => "Pour",
	"Disagree" => "Contre",
	"Abstain" => "Blanc",
	"Uncomplet" => "Incomplet",

	"number of voters" => "Nombre de votant(s)",
	"Your amendement has been save with success" => "Votre amendement a été enregistré avec succès",
	"Your vote has been save with success" => "Votre vote a été enregistré avec succès",
	"processing save" => "Sauvegarde en cours",
	"processing delete" => "Suppression en cours",
	"processing delete ok" => "Suppression effectuée",
	"processing delete KO" => "Erreur lors de la suppression",
	"You already voted the same way" => "Vous avez déjà voté",
	"Refresh data" => "Actualiser l'espace CO",
	"processing" => "Chargement en cours",
	"processing ok" => "Chargement terminé",
	"all status" => "en vrac",

	"in this space" => "dans l'espace",
	"The final vote will be on the content of your proposal" => "Le vote final portera sur le contenu de votre proposition.",
	"For clarity, please provide additional information about your proposal in the next section." => "Pour plus de clareté, détaillez toute information complémentaire, relative à votre proposition, dans la section suivante.",
	"A proposal is used to discuss and seek community input on a given idea or question" => "Une proposition sert à discuter et demander l'avis d'une communauté sur une idée ou une question donnée",

	"Votes are disabled during the amendment period" =>" Les votes sont désactivés pendant la période d'amendement",
	"If Yes, votes are enabled during the amendment period" =>"Si oui, les votes sont activés pendant la période d'amendement",

	"Enable amendments" =>" Activer les amendements",
	"Amendments and votes at the same time" => "Les amendements et les votes en même temps",

	"anonymous" => "anonyme",
	"nominative" => "nominatif",

	"activated" => "activés",
	"disabled" => "désactivés",

	"Rule of majority" => "Règle de majorité",
	"give a value between 50% and 100%" => "indiquez une valeur entre 50% et 100%",

	"Anonymous votation" => "Votes anonyme",

	"Do you want to keep secret voters identity ?" => "Souhaitez-vous garder secrète l'identité des votants ?",

	"changing vote enabled" => "changement de vote autorisé",
	"changing vote forbiden" => "changement de vote interdit",

	"Authorize changing vote ?" => "Autoriser le changement de vote ?",
	"Do you want to allow voters to change them vote ?" => "Souhaitez-vous permettre aux votants de changer leur choix à volonté ?",

	"Your proposal must have more than " => "Votre proposition devra recueillir plus de ",
	"of votes" => "de votes",
	"favorables" => "favorables",
	"to be validated" => "pour être validée",

	"Impossible to go over 100%" => "Impossible de dépasser les 100%",
	"Impossible to go under 50%" => "Impossible de descendre à moins de 50%",

	"Date end vote session" => "Fin de la période de vote",
	"Date end amendement session (start of vote session)" => "Fin de la période d'amendement (ouverture des votes)",
	"Date end amendement session" => "Fin de la période d'amendement",

	"Edit my proposal" => "Modifier ma proposition",
	"Back to amendements" => "Retour aux amendements",
	"Open votes" => "Ouvrir les votes",
	"Disabled my proposal" => "Désactiver ma proposition",
	"Close my proposal" => "Fermer ma proposition",
	"Delete my proposal" => "Effacer ma proposition",

	"Edition disabled" => "Modifications désactivées",
	"amendement session has begun" => "les amendements ont commencé",
	"vote session has begun" => "les votes ont commencé",

	"Update datas" => "Actualiser les données",
	"Enlarge reading space" => "Agrandir l'espace de lecture",
	"Reduce reading space" => "Réduire l'espace de lecture",
	"your are the author of this proposal" => "vous êtes l'auteur de cette proposition",
	"is the author of this proposal" => "est l'auteur de cette proposition",
	"Close this window" => "Fermer cette fenêtre",
	"You are not allowed to access this content" => "Vous n'êtes pas autorisé à accéder à ce contenu",

	"You must be member or contributor" => "Devenez membre ou contributeur",
	"Login to enter in this space" => "Connectez-vous pour entrer dans cet espace",
	"Proposal submited to amendements" => "Proposition soumise aux amendements",

	"until" => "jusqu'au",
	"End of amendement session" => "Fin des amendements",
	"You can submit your amendements and vote amendement proposed by other users" => "Vous pouvez proposer des amendements et voter les amendements proposés par les autres utilisateurs",
	
	"Proposal" => "Proposition",
	"temporaly" => "temporairement",
	"definitively" => "définitivement",
	"Validated" => "Validée",
	"Refused" => "Refusée",
	"validated" => "validée",
	"refused" => "refusée",
	"adopted" => "validée",
	"adopteds" => "adoptées",
	"voter" => "votant",
	// pour défirencier le masculin est le féminin
	"Validated2" => "Validé",
	"Refused2" => "Refusé",
	"validated2" => "validé",
	"refused2" => "refusé",
	"adopted2" => "validé",
	"adopteds2" => "adoptés",

	"Be the first to vote" => "Soyez le premier à voter",
	"Vote open until undefined date" => "Vote ouvert jusqu'à une date non-définie",

	"End of vote session" => "fin du vote",
	"You did vote" => "Vous avez voté",
	"You did not vote" => "Vous n'avez pas voté",
	"You did not vote yet" => "Vous n'avez pas encore voté",
	"See votes" => "Voir les votes",
	"You voted for this answer" => "Vous avez choisi cette réponse",
	"The <b>resolution</b> is done : " => "La <b>résolution</b> suivante a été prise : ",
	"The proposal is" => "la proposition est",
	"Show the resolution" => "Afficher la résolution",

	"Submit an amendement" => "Proposer un amendement",
	"Amendement session is closed" => "La période d'amendement est terminée",

	"to submit amendements" => "pour proposer des amendements",

	"Show amendements" => "Afficher les amendements",
	"Show all amendements" => "Afficher tous les amendements",
	"List of amendements" => "Liste des amendements",
	"Majority" => "Majoritée",
	"Add" => "Ajout",
	"No amendement" => "Aucun amendement",
	"No amendement validated" => "Aucun amendement validé",
	"Amendement disabled" => "Amendements désactivés",
	"More informations, arguments, exemples, demonstrations, etc" => "Compléments d'informations, argumentations, exemples, démonstrations, etc",
	"External links" => "Liens externes",
	"Debat" => "Débat",
	"Add an argument" => "Ajouter un argument",
	"Add an action" => "Ajouter une action",

	"For" => "Pour",
	"Neutral" => "Neutre",
	"Against" => "Contre",

	"I agree" => "Pour",
	"I disagree" => "Contre",

	"You must be member or contributor to participate" => "Devenez membre ou contributeur pour participer au débat",


	"Edit this space" => "modifier l'espace",
	"You are not logged" => "Vous n'êtes pas connecté⋅e",
	"My roles" => "Mes rôles",
	"You have no role in" => "Vous n'avez aucun rôle dans",
	"this projects" => "ce projet",
	"this organizations" => "cette organisation",
	"Ask an admin to get the appropriated role to access this space" => 
		"Demandez à un administrateur de vous donner le rôle approprié pour accéder à cet espace",
	"This space is open only for this roles" => "Cet espace est réservé au(x) rôle(s) suivant(s)",
	"You must be member or contributor to contribuate" => "Devenez membre pour contribuer",
	"Drag / drop to an other space" => "cliquer / déplacer dans un autre espace",
	"Processing amendements" => "En cours d'amendement",
	"No vote" => "Aucun vote",
	"Reload window" => "Rafraichir la page",
	"Show vote details" => "Afficher les détails du vote",


	"You must be member or contributor to vote" => "Devenez membre ou contributeur pour voter",

	"votes are anonymous" => "les votes sont anonymes",
	"You can not change your vote" => "Vous ne pouvez plus changer votre vote",

	"Changing vote" => "Changement de vote",

	"Allowed" => "Autorisé",
	"Not allowed" => "Non-autorisé",

	"VOTE NOW" => "VOTER MAINTENANT",
	"RESULTS" => "RÉSULTATS",

	"List of actions linked with this resolution" => "Liste des actions liées à cette résolution",

	"No action for this resolution" => "Aucune action liée à cette résolution",

	"Access restricted only for" => "Accès réservé",

	"Your amendement is too short ! Minimum : 10 caracters" => "Votre amendement est trop court ! Minimum : 10 caractères",

	"Thanks for your participation !" => "Merci pour votre participation !",

	"Loading comments" => "Chargement des commentaires",


	"Amendements" => "Amendements",
	"Write your amendement" => "Rédigez votre amendement",
	"If your amendement is adopted, it will be added to the principale proposal, <br>and will incorporated the final proposal, submited to vote." => 
	"Si votre amendement est accepté, il sera ajouté à la suite de la proposition principale,<br>et fera partie de la proposition finale, soumise au vote.",

	"your amendement" => "votre amendement",
	"Size max : 1000 caracters" => "Les amendements ne peuvent dépasser la taille de 1000 caractères",

	"Size min : 10 caractères" => "taille minimale : 10 caractères",
	"Save my amendement" => "Enregistrer mon amendement",

	"You can change your vote anytime" => "Vous pouvez changer votre choix à tout moment",

	"Vote open until" => "Le vote est ouvert jusqu'au",

	"voters" => "votants",
	"Vote" => "Votez",

	"click to vote" => "cliquer pour voter",
	"number of voters" => "nombre de votants",
	"Delete my amendement" => "Effacer mon amendement",

	"Collective moderation" => "Modération collective",

	"open in CO space" => "ouvrir dans l'espace CO",

	"Survey in process" => "Sondage en cours",

	"The <b>vote</b> is ended : " => "Le <b>vote</b> est terminé",

	"Answer" => "Réponse",
	"the answer" => "la réponse",
	"No answer" => "Aucune réponse",

	"click to vote" => "cliquer pour voter",

	"Please login to post a message" => "Merci de vous connecter pour publier un message",
	"Please login to vote" => "Connectez vous pour voter",
	"I'm logging in" => "Je me connecte",
	"I create my account" => "Je créé mon compte",
	"free registration" => "inscription gratuite",

	"Chat"=>"Messagerie",

	"create a survey" => "créer un sondage",
	"Survey name" => "Titre du sondage",
	"Survey text" => "Votre texte",

	"Surveys are published for all your followers" => "Les sondages sont automatiquement partagés avec tous vos abonnés",

	"Add measure" => "Ajouter une mesure",
	"Size min : 10 caracters"=> "Taille minimun : 10 caractères",
	"Invite to vote" => "Inviter à voter",
	"Welcome in your cooperative space" =>"Bienvenue dans votre espace coopératif",
	"What is it useful ?"=>"A quoi ça sert ?",
	"This space is design to offer a tool for <b>collective organization</b> in order to have transparency in the common decisions"=>
		"L'espace coopératif peut être considéré comme un outil de gestion de projet collaboratif, permettant de mettre en place une forme de <b>gouvernance transparente et horizontale</b>",
	"You can submit proposals to concert {what}"=>"C'est un outil d'aide à la décision, qui vous permettra de prendre des <b>décisions collectives</b>, en concertation avec {what}",
	"all the members of your organization"=>"l'ensemble des membres de votre organisation",
	"all the contributors of the project"=>"l'ensemble des contributeurs de votre projet",
	"It will permit to deal with differents actions (taks) to realize in your activity and attribute them to the {who}"=>"
		Il vous permettra également de gérer les différentes <b>actions</b> (tâches) à réaliser dans le cadre de votre activité,
		et d'attribuer ces actions à vos {who}",
	"How does it work ?"=>"Comment ça marche ?",
	"Because {who}, you start with thematic space depending on your needs"=>"Parce que {who}, vous commencerez par créer des espaces thématiques en fonction de vos besoins",
	"each project is different"=>"projet est différent",
	"each organization is different"=>"organisation est différente",
	"For instance, if you deal with a sport club, you can create a space named 'depense about the club', inside the members can add their proposals link to this theme"=>"Par exemple, si vous gérez un club sportif, vous pourrez créer un espace nommé 'Utilisation du bugdet du club', dans lequel vos adhérents pourront faire leurs propositions en lien avec ce thème",
	"Each space created will be receive proposals and actions linked to its theme"=>"Chaque espace ainsi créé peut recevoir des propositions et des actions, en lien avec le thème de l'espace",
	"Let's start !"=>"C'est parti !",
	"Create your first room clicking on the following button"=>"Créez votre premier espace coopératif, en cliquant sur le bouton",
	"You will find this button again in the left menu of your screen"=>"(Vous retrouverez ce bouton dans le menu situé à gauche de votre écran)",
	"Your new room will appear on the left menu too, and you will be able to add your first proposal clicking on this button"=>"Votre nouvel espace s'affichera dans le menu de gauche, et vous pourrez immédiatement faire votre première <b>proposition</b> en cliquant sur le bouton",
	"Proposal, Amendment, Vote..."=>"Proposer, amender, voter...",
	"This is the 3 steps of collective decision processus we offer you"=>"Ce sont les 3 étapes incontournables du processus de décision collective que nous vous proposons",
	"<b>1 - Proposal :</b> a proposal is writing by {who}"=>"<b>1 - Proposer :</b> Une proposition est un texte écrit par {who}",
	"a member of the organization"=>"un membre de votre organisation",
	"a contributor of the project"=>"un contributeur de votre projet",
	"The author can activate or desactivate the <i>process of amendment</i>. He choose also the date limit of survey"=>"L'auteur d'une proposition peut activer ou désactiver la <i>procédure d'amendement</i>, selon la nécessité ou non de celle-ci. Il en définit également la durée.<br>L'auteur définit également la durée de la <i>procédure de vote</i>, plus ou moins longue en fonction du besoin de reflexion collective autour du sujet proposé",
	"<b>2 - Amend :</b> an amendment is an enhancement, submit to collective decision, in order to correct, complete or cancel all or a part of the proposal in process.<br/><i>(Currently, it is only possible to add proposal added informations. The update and and the delete is not already develop)</i>"=>"<b>2 - Amender :</b> Un amendement est une modification, soumise au vote, dont le but est de corriger, compléter ou annuler tout ou une partie de la proposition en cours de délibération.<br>
				<i>(actuellement, il est seulement possible de compléter la proposition par ajout d'information. La modification, et suppression, ne sont pas encore disponibles)</i>",
	"{who} can purpose an amendment on proposals<br/>Each amendment is submit to vote.<br/>When amendment time limit is over, validating amendment are automatically adding to the original proposal and the vote on the proposal starts."=>"{who} peuvent proposer des amendements aux propositions.<br>Chaque amendement est soumi au vote.<br>Lorsque la période d'amendement est achevée, les amendements validés par le vote sont automatiquement ajouté à la proposition originale, et la période de vote commence.",
	"All contributors"=>"Tous les contributeurs",
	"All members"=>"Tous les membres",
	"Notice : Amendment period can be desactivated by the proposal's author to launch directly vote process"=>"Rappel : la période d'amendement peut être désactivée par l'auteur de la proposition, afin de lancer directement la procédure de vote",
	"<b>3 - Voting :<b> When potential amendment time is finished, voting is starting and you community can give its opinion"=>"<b>3 - Voter :</b> Lorsque la période d'amendement est terminée (ou désactivée), la période de vote commence.<br/>Chacun peut alors donner son avis en votant",
	"And then"=>"Et après",
	"When voting time is over, the proposal is closed, and becomes a <i>resolution</i><br/>You can find out all resolutions <b class='letter-green'>accepted</b> or <b class='letter-red'>refused</b> in each room in the following section :"=>
		"Lorsque la période de vote est terminée, la proposition est automatiquement fermée, puis transformée en <i>résolution</i>.<br>Vous pouvez retrouver l'ensemble des résolutions <b class='letter-green'>adoptées</b> ou <b class='letter-red'>refusées</b> de chaque espace dans la section suivante :",
	"And about actions ?"=> "Et les actions dans tout ça ?",
	"Actions can be freely created in each room<br/>They can be correlated to a proposal"=>"Les actions peuvent être créées librement dans chaque espace, en fonction de vos besoins.<br>Elles peuvent aussi être créées directement à la suite de chaque proposition adoptée, pour mettre en application les décisions",
	"Delete a room"=> "Supprimer un espace coopératif",
	"Are you sure to delete this cooperative space ?"=>"Etes-vous sur de vouloir supprimer cet espace coopératif ?",
	"All the proposals, resolutions and actions linked to this room will be deleted"=>"Toutes les propositions, résolutions, et actions de cet espace seront supprimées définitivement",
	"Yes, delete this room"=>"Oui, supprimer cet espace",
	"Delete the room : {what}"=>"supprimer l'espace : {what}",
	"Add document"=> "Ajouter un document",
	"Search in documents"=>"Recherche dans les documents",
	"No documents"=>"Aucun document",
);
