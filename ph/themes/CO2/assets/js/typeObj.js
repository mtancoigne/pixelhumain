window.emojiconReactions = [
    { "emocion": "love", "TextoEmocion": trad.ilove, "class": "amo", "color": "text-red" },
    { "emocion": "bothered", "TextoEmocion": trad.bothering, "class": "molesto", "color": "text-orange" },
    { "emocion": "scared", "TextoEmocion": trad.what, "class": "asusta", "color": "text-purple" },
    { "emocion": "enjoy", "TextoEmocion": trad.toofunny, "class": "divierte", "color": "text-orange" },
    { "emocion": "like", "TextoEmocion": "+1", "class": "gusta", "color": "text-red" },
    { "emocion": "sad", "TextoEmocion": trad.sad, "class": "triste", "color": "text-azure" },
    { "emocion": "support", "TextoEmocion": trad.isupport, "class": "support", "color": "letter-blue" },
    { "emocion": "glad", "TextoEmocion": trad.cool, "class": "alegre", "color": "text-orange" },
    { "emocion": "disguted", "TextoEmocion": trad.burk, "class": "disguted", "color": "text-brown" }
];

window.typeObj = {
    addPhoto: {
        titleClass: "dark", color: "dark", bgClass: "bgPerson",
        formType: "addPhoto",
        name: "Ajouter une photo",
        createLabel: "Ajout de photo d'albums",
        explainText: "Blabla"
    },
    addFile: {
        titleClass: "dark", color: "dark",
        formType: "addFile",
        name: "Ajouter un fichier",
        createLabel: "Télécharger des fichiers",
        explainText: "Blabla"
    },
    photo: { titleClass: "bg-dark", color: "bg-dark" },
    file: { titleClass: "bg-dark", color: "bg-dark" },
    person: {
        col: "citoyens",
        ctrl: "person", titleClass: "bg-yellow", bgClass: "bgPerson", color: "yellow", icon: "user", hash: "#element.invite",
        class: "lbhp",
        add: true,
        addInElement: false,
        name: trad.people,
        addLabel: trad.invitesomeone,
        createLabel: trad.invitesomeone,
        /*explainText:"Diffuse an event<br>Invite attendees<br>Communicate to your network",*/
    },
    persons: { sameAs: "person" },
    people: { sameAs: "person" },
    citoyen: { sameAs: "person" },
    citoyens: { sameAs: "person" },

    siteurl: { col: "siteurl", ctrl: "siteurl" },
    organization: {
        col: "organizations", ctrl: "organization", icon: "group", titleClass: "bg-green", color: "green", bgClass: "bgOrga",
        add: true,
        addInElement: false,
        formType: "organization",
        name: trad.organization,
        createLabel: trad.createorganization,
        explainText: "Blabla"
    },
    organizations: { sameAs: "organization" },
    organization2: { col: "organizations", ctrl: "organization" },
    LocalBusiness: {
        col: "organizations", color: "azure", icon: "industry",
        name: trad.LocalBusiness,
        addInElement: true,
        formType: "organization",
        formSubType: "LocalBusiness",
        createLabel: "Create a local business",
        explainText: tradDynForm.infosmallcreatebusiness,
        parentAllow: ["citoyens"]
    },
    NGO: {
        sameAs: "organization", color: "green", icon: "users",
        name: trad.NGO,
        formType: "organization",
        createLabel: "Create an NGO",
        formSubType: "NGO",
        addInElement: true,
        explainText: tradDynForm.infosmallcreatengo,
        parentAllow: ["citoyens"]
    },
    Association: { sameAs: "organization", color: "green", icon: "group" },
    GovernmentOrganization: {
        col: "organization", color: "red", icon: "university",
        name: trad.GovernmentOrganization,
        formType: "organization",
        formSubType: "GovernmentOrganization",
        createLabel: "Create a public sevrice",
        addInElement: true,
        explainText: tradDynForm.infosmallcreatepublicservice,
        parentAllow: ["citoyens"]
    },
    Group: {
        col: "organizations", color: "turq", icon: "circle-o",
        name: trad.Group,
        formType: "organization",
        formSubType: "Group",
        createLabel: "Create a group",
        addInElement: true,
        explainText: tradDynForm.infosmallcreategroup,
        parentAllow: ["citoyens"]
    },
    event: {
        col: "events", ctrl: "event", icon: "calendar", titleClass: "bg-orange", color: "orange", bgClass: "bgEvent",
        add: true,
        addInElement: true,
        formType: "event",
        name: trad.event,
        createLabel: trad.createevent,
        explainText: tradDynForm.infosmallcreateevent,
        parentAllow: ["citoyens", "organizations", "projects", "events"]
    },

    events: { sameAs: "event" },
    project: {
        col: "projects", ctrl: "project", icon: "lightbulb-o", color: "purple", titleClass: "bg-purple", bgClass: "bgProject",
        add: true,
        addInElement: true,
        formType: "project",
        name: trad.project,
        createLabel: trad.createproject,
        explainText: tradDynForm.infosmallcreateproject,
        parentAllow: ["citoyens", "organizations", "projects"]
    },
    projects: { sameAs: "project" },
    project2: { col: "projects", ctrl: "project" },
    city: { sameAs: "cities" },
    cities: { col: "cities", ctrl: "city", titleClass: "bg-red", icon: "university", color: "red" },

    entry: {
        col: "surveys", ctrl: "survey", titleClass: "bg-dark", bgClass: "bgDDA", icon: "gavel", color: "azure",
        saveUrl: baseUrl + "/" + moduleId + "/survey/saveSession"
    },

    product: { col: "products", ctrl: "product", titleClass: "bg-orange", color: "orange", icon: "shopping-basket" },
    products: { sameAs: "product" },
    service: { col: "services", ctrl: "service", titleClass: "bg-green", color: "green", icon: "sun-o" },
    services: { sameAs: "service" },
    circuit: { col: "circuits", ctrl: "circuit", titleClass: "bg-orange", color: "green", icon: "ravelry" },
    circuits: { sameAs: "circuit" },
    poi: {
        col: "poi", ctrl: "poi", color: "green-poi", titleClass: "bg-green-poi", icon: "map-marker",
        subTypes: ["link", "tool", "machine", "software", "rh", "RessourceMaterielle", "RessourceFinanciere",
            "ficheBlanche", "geoJson", "compostPickup", "video", "sharedLibrary", "artPiece", "recoveryCenter",
            "trash", "history", "something2See", "funPlace", "place", "streetArts", "openScene", "stand", "parking", "other"],
        add: true,
        addInElement: true,
        name: tradCategory.poi,
        formType: "poi",
        explainText: tradDynForm.infosmallcreatepoi,
        parentAllow: ["citoyens", "organizations", "projects", "events"]
    },
    url: { col: "url", ctrl: "url", titleClass: "bg-blue", bgClass: "bgPerson", color: "blue", icon: "user", saveUrl: baseUrl + "/" + moduleId + "/element/saveurl", },
    bookmark: { col: "bookmarks", ctrl: "bookmark", titleClass: "bg-dark", bgClass: "bgPerson", color: "blue", icon: "bookmark" },
    document: { col: "document", ctrl: "document", titleClass: "bg-dark", bgClass: "bgPerson", color: "dark", icon: "upload", saveUrl: baseUrl + "/" + moduleId + "/element/savedocument", },
    default: { icon: "arrow-circle-right", color: "dark" },
    //"video" : {icon:"video-camera",color:"dark"},
    formContact: { titleClass: "bg-yellow", bgClass: "bgPerson", color: "yellow", icon: "user", saveUrl: baseUrl + "/" + moduleId + "/app/sendmailformcontact" },
    news: { col: "news", ctrl: "news", titleClass: "bg-dark", color: "dark", icon: "newspaper-o" },
    //news : { col : "news" }, 
    config: {
        col: "config", color: "azure", icon: "cogs", titleClass: "bg-azure", title: tradDynForm.addconfig,
        sections: {
            network: { label: "Network Config", key: "network", icon: "map-marker" }
        }
    },

    classified: {
        col: "classifieds", ctrl: "classified", color: "azure", icon: "bullhorn", titleClass: "bg-azure", bgClass: "bgPerson",
        add: true,
        addInElement: true,
        formType: "classifieds",
        name: trad.classified,
        createLabel: tradDynForm.addclassified,
        explainText: tradDynForm.infosmallcreateclassifieds,
        parentAllow: ["citoyens", "organizations", "projects"]
    },
    classifieds: { sameAs: "classified" },
    ressource: {
        col: "classifieds", ctrl: "classified", color: "vine", icon: "cubes", titleClass: "bg-vine", bgClass: "bgPerson",
        add: true,
        addInElement: true,
        formType: "ressources",
        name: trad.ressource,
        createLabel: "add a ressource",
        explainText: tradDynForm.infosmallcreateressources,
        parentAllow: ["citoyens", "organizations", "projects", "events"]
    },
    ressources: { sameAs: "ressource" },
    job: {
        col: "classifieds", ctrl: "classified", color: "yellow-k", icon: "briefcase", titleClass: "bg-yellow-k", bgClass: "bgPerson",
        add: true,
        addInElement: true,
        formType: "jobs",
        name: trad.job,
        createLabel: "Add an offers",
        explainText: tradDynForm.infosmallcreatejobs,
        parentAllow: ["citoyens", "organizations", "projects"]
    },
    jobs: { sameAs: "job" },
    network: { col: "network", color: "azure", icon: "map-o", titleClass: "bg-turq" },
    networks: { sameAs: "network" },
    vote: { sameAs: "proposals" },
    survey: { col: "proposals", ctrl: "proposal", color: "dark", icon: "hashtag", titleClass: "bg-turq" },
    surveys: { sameAs: "survey" },
    proposal: {
        col: "proposals", ctrl: "proposal", color: "turq", icon: "gavel", titleClass: "bg-turq",
        add: true,
        addInElement: true,
        name: trad.survey,
        formType: "proposal",
        createLabel: "Create a survey",
        explainText: tradDynForm.infosmallcreatesurvey,
        parentAllow: ["organizations", "projects"]
    },
    proposals: { sameAs: "proposal" },
    proposal2: { sameAs: "proposal" },
    resolutions: { col: "resolutions", ctrl: "resolution", titleClass: "bg-turq", bgClass: "bgDDA", icon: "certificate", color: "turq" },
    action: { col: "actions", ctrl: "action", titleClass: "bg-turq", bgClass: "bgDDA", icon: "cogs", color: "dark" },
    actions: { sameAs: "action" },
    actionRooms: { sameAs: "room" },
    rooms: { sameAs: "room" },
    room: { col: "rooms", ctrl: "room", color: "azure", icon: "connectdevelop", titleClass: "bg-turq", formType: "room" },
    discuss: { col: "actionRooms", ctrl: "room" },
    contactPoint: {
        col: "contact", ctrl: "person", titleClass: "bg-blue", bgClass: "bgPerson", color: "blue", icon: "user",
        saveUrl: baseUrl + "/" + moduleId + "/element/saveContact"
    },
    contacts: {
        color: "blue", icon: "envelope", titleClass: "bg-blue",
        add: false,
        addInElement: true,
        name: trad.contact,
        formType: "contactPoint",
        createLabel: "Add a contact",
        explainText: tradDynForm.infosmallcreatecontact,
        parentAllow: ["organizations", "projects"]
    },
    curiculum: { color: "dark", icon: "clipboard", titleClass: "bg-dark", title: "My CV" },
    badge: {
        col: "badges", color: "dark", icon: "bookmark", titleClass: "bg-dark",
        name: "Badge",
        add: false,
        formType: "badge",
        createLabel: "Ajouter un badge",
        explainText: "Proposer les badges domaines d'action et cible de développement durable<br/> qui permettront aux projets menés sur les territoires de se définir et ainsi de permettre de construire l'orientation stratégique des CTE",

    },
    badges: { sameAs: "badge" },
    costum: { col: "costum", color: "dark", icon: "photo", titleClass: "bg-dark", title: "Costum" },

    get: function (e) {
        // mylog.log("typeObj.get", e);
        elt = {};
        if (typeof typeObj[e] != "undefined") {
            /* var setTypeObj=typeObj[e];
             if(typeof  typeObj[e].sameAs != "undefined"){
               var setTypeObjSameAs=jQuery.extend(true, {},typeObj[typeObj[e].sameAs]);
             }
             //console.log("glouuuuuuuuuuuuuu",setTypeObj, setTypeObjSameAs);
             $.each(["name", "color", "icon", "formType", "formSubType", "createLabel", "col", "ctrl","formParent", "dynFormCostum","formType"], function(e,v){
               if(typeof setTypeObj[v] != "undefined")
                 elt[v]=setTypeObj[v];
               else if (!inArray(v,["dynForm", "formParent"]) && typeof setTypeObjSameAs != "undefined" && typeof setTypeObjSameAs[v] != "undefined"){
                     elt[v]=setTypeObjSameAs[v];
                 }
             });*/
            if (typeof typeObj[e].name != "undefined")
                elt.name = typeObj[e].name;
            else if (typeof typeObj[e].sameAs != "undefined")
                elt.name = typeObj[typeObj[e].sameAs].name;

            if (typeof typeObj[e].icon != "undefined")
                elt.icon = typeObj[e].icon;
            else if (typeof typeObj[e].sameAs != "undefined")
                elt.icon = typeObj[typeObj[e].sameAs].icon;

            if (typeof typeObj[e].color != "undefined")
                elt.color = typeObj[e].color;
            else if (typeof typeObj[e].sameAs != "undefined")
                elt.color = typeObj[typeObj[e].sameAs].color;

            if (typeof typeObj[e].formType != "undefined")
                elt.formType = typeObj[e].formType;
            else if (typeof typeObj[e].sameAs != "undefined")
                elt.formType = typeObj[typeObj[e].sameAs].formType;

            if (typeof typeObj[e].formParent != "undefined")
                elt.formParent = typeObj[e].formParent;

            if (typeof typeObj[e].dynForm != "undefined")
                elt.dynForm = typeObj[e].dynForm;

            if (typeof typeObj[e].dynFormCostum != "undefined")
                elt.dynFormCostum = typeObj[e].dynFormCostum;
            else if (typeof typeObj[e].sameAs != "undefined" && typeof typeObj[typeObj[e].sameAs].dynFormCostum != "undefined")
                elt.dynFormCostum = typeObj[typeObj[e].sameAs].dynFormCostum;

            if (typeof typeObj[e].formSubType != "undefined")
                elt.formSubType = typeObj[e].formSubType;
            else if (typeof typeObj[e].sameAs != "undefined" && typeof typeObj[typeObj[e].sameAs].formSubType != "undefined")
                elt.formSubType = typeObj[typeObj[e].sameAs].formSubType;

            if (typeof typeObj[e].createLabel != "undefined")
                elt.createLabel = typeObj[e].createLabel;
            else if (typeof typeObj[e].sameAs != "undefined")
                elt.createLabel = typeObj[typeObj[e].sameAs].createLabel;

            if (typeof typeObj[e].col != "undefined")
                elt.col = typeObj[e].col;
            else if (typeof typeObj[e].sameAs != "undefined")
                elt.col = typeObj[typeObj[e].sameAs].col;

            if (typeof typeObj[e].ctrl != "undefined")
                elt.ctrl = typeObj[e].ctrl;
            else if (typeof typeObj[e].sameAs != "undefined")
                elt.ctrl = typeObj[typeObj[e].sameAs].ctrl;
        }
        //mylog.log("typeObj.get end elt", elt);
        return elt;
    },
    isDefined: function (type, entry, obj) {
        res = true;
        if (notNull(obj))
            inspector = obj;
        else if (typeof typeObj[type] != "undefined")
            inspector = typeObj[type];
        else
            res = false;
        if (res) {
            checkValues = (entry.indexOf(".")) ? entry.split(".") : [entry];
            $.each(checkValues, function (e, v) {
                if (typeof inspector[v] != "undefined") {
                    res = true;
                    inspector = inspector[v];
                } else
                    res = false;
            });
        }
        //if(notNull(entry) && res)
        //  res = (typeof typeObj[type][entry] != "undefined") ? true : false;
        //}
        return res;
    },
    authorizedButton: function (elt, config, context) {
        //if( typeof context != "undefined")
        //  mylog.log("authorizedButton",elt, config, context);
        auth = false;
        if (notNull(context) && typeof context != "undefined") {
            if (elt.add == "onlyMember" &&
                (typeof context.isMember != "undefined" && context.isMember === true))
                auth = true;
            else if (elt.add == "onlyAdmin" && typeof canCreate != "undefined" && canCreate)
                auth = true;
            else if (elt.add === true)
                auth = true;
        } else if (notNull(config) && typeof config.inElement != "undefined") {
            if (typeof elt.addInElement != "undefined" && elt.addInElement) {
                auth = true;
                if (typeof config.allowIn != "undefined" && config.allowIn
                    && typeof elt.parentAllow != "undefined" && $.inArray(config.contextType, elt.parentAllow) < 0)
                    auth = false;
            }
            if (typeof elt.add != "undefined" && (!elt.add || (elt.add == "onlyAdmin" && (typeof canCreate == "undefined" || !canCreate))))
                auth = false;
        } else if (typeof elt.add != "undefined") {
            if (elt.add == "onlyAdmin" && typeof canCreate != "undefined" && canCreate)
                auth = true;
            else if (elt.add === true)
                auth = true;
        }
        return auth;
    },
    buildCreateButton: function (domContain, dropdownButton, params, context, filterType) {
        menuButtonCreate = "";
        var count = 0;
        var hash = "";
        var formType = "";
        var subFormType = "";
        var addClass = "";
        var nameLabel = "";
        var bgClass = "";
        var textExplain = "";
        $.each(typeObj, function (e, v) {
            if (typeObj.authorizedButton(v, params, context) &&
                ( typeof filterType == "undefined" || 
                    ( typeof filterType == "object" && $.inArray(v.col, filterType) > -1 ) ) ) {
                mylog.log("buildCreateButton", v, params);
                count++;
                hash = (typeof v.hash != "undefined") ? v.hash : "javascript:;";
                openFormClass = (typeof v.hash == "undefined") ? "btn-open-form" : "";
                formType = (typeof v.formType != "undefined") ? 'data-form-type="' + v.formType + '" ' : "";
                subFormType = (typeof v.formSubType != "undefined") ? 'data-form-subtype="' + v.formSubType + '" ' : "";
                addClass = (typeof v.class != "undefined") ? v.class : "";
                addClass += " " + openFormClass;
                nameLabel = (typeof v.addLabel != "undefined") ? v.addLabel : v.name;
                bgIcon = "";
                bgClass = "bg-" + v.color;
                inline = "";
                if (notNull(params)) {
                    bgClass = (typeof params.bgColor != "undefined") ? "bg-" + params.bgColor : bgClass;
                    textExplain = (typeof params.explain != "undefined") ? "<small>" + v.explainText + "</small>" : "";
                    addClass = (typeof params.addClass != "undefined") ? params.addClass : addClass;
                    addClass = (typeof params.textColor != "undefined") ? addClass + " text-" + v.color : addClass;
                    bgIcon = (typeof params.bgIcon != "undefined") ? "bg-" + v.color : bgIcon;
                    inline = (typeof params.inline != "undefined" && !params.inline) ? "<br/>" : "";
                }
                menuButtonCreate += '<a href="' + hash + '" ' +
                    formType +
                    subFormType +
                    'class="addBtnFoot btn btn-default ' + addClass + ' ' + bgClass + ' margin-bottom-10">' +
                    '<i class="fa fa-' + v.icon + ' ' + bgIcon + '"></i>' + inline + ' <span>' + nameLabel + '</span>' +
                    inline + textExplain
                '</a>';
            }
        });
        //mylog.log("buildCreateButton menuButtonCreate", menuButtonCreate);
        $(domContain).html(menuButtonCreate);
        if (count <= 1 && notNull(dropdownButton) && dropdownButton) {
            oneButton = '<a href="' + hash + '" ' +
                formType +
                subFormType +
                'class="btn btn-default no-padding btn-menu-vertical ' + addClass + '" id="show-bottom-add">' +
                '<i class="fa fa-plus-circle"></i>' +
                '<span class="tooltips-menu-btn">' + nameLabel + '</span>' +
                '</a>';
            $("#show-bottom-add").replaceWith(oneButton);
        }
    }
};


window.themeObj = {
    init: function (noLoading) {
        mapCO = mapObj.init(paramsMapCO);
        allMaps.maps[paramsMapCO.container] = mapCO;
        toastr.options = {
            "closeButton": false,
            "positionClass": "toast-bottom-right",
            "onclick": null,
            "showDuration": "1000",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        if (typeof coInterface.simpleScroll != "undefined") coInterface.simpleScroll(0);
        if (typeof typeObj.buildCreateButton != "undefined") typeObj.buildCreateButton(".toolbar-bottom-adds", true);
        if (typeof initFloopDrawer != "undefined") initFloopDrawer();
        if (typeof themeObj.initMyScopes != "undefined") themeObj.initMyScopes();
        if (typeof coInterface.init != "undefined") coInterface.init();
        //Init mentions contact
        if (myContacts != null) {
            $.each(myContacts["citoyens"], function (key, value) {
                if (typeof (value) != "undefined") {
                    avatar = "";
                    if (value.profilThumbImageUrl != "")
                        avatar = baseUrl + value.profilThumbImageUrl;
                    object = new Object;
                    object.id = value._id.$id;
                    object.name = value.name;
                    object.slug = value.slug;
                    object.avatar = avatar;
                    object.type = "citoyens";
                    mentionsContact.push(object);
                }
            });
            $.each(myContacts["organizations"], function (key, value) {
                if (typeof (value) != "undefined") {
                    avatar = "";
                    if (value.profilThumbImageUrl != "")
                        avatar = baseUrl + value.profilThumbImageUrl;
                    object = new Object;
                    object.id = value._id.$id;
                    object.name = value.name;
                    object.slug = value.slug;
                    object.avatar = avatar;
                    object.type = "organizations";
                    mentionsContact.push(object);
                }
            });
            $.each(myContacts["projects"], function (key, value) {
                if (typeof (value) != "undefined") {
                    avatar = "";
                    if (value.profilThumbImageUrl != "")
                        avatar = baseUrl + value.profilThumbImageUrl;
                    object = new Object;
                    object.id = value._id.$id;
                    object.name = value.name;
                    object.slug = value.slug;
                    object.avatar = avatar;
                    object.type = "projects";
                    mentionsContact.push(object);
                }
            });
        }
        urlCtrl.bindCoNav();

    },
    firstLoad: (costum != null && costum.themeObj != null && costum.themeObj.firstLoad != null) ? costum.themeObj.firstLoad : true,
    imgLoad: "CO2r.png",
    mainContainer: ".pageContent",
    blockUi: {
        setLoader: function () {
            color1 = "#354c57";
            color2 = "#e6344d";
            logoLoader = themeUrl + '/assets/img/LOGOS/' + domainName + '/logo.png';
            if (notNull(costum)) {
                logoLoader = costum.logo;
                if (typeof costum.css != "undefined" && typeof costum.css.loader != "undefined") {
                    if (typeof costum.css.loader.ring1 != "undefined" && costum.css.loader.ring1.color != "undefined")
                        color1 = costum.css.loader.ring1.color;
                    if (typeof costum.css.loader.ring2 != "undefined" && costum.css.loader.ring2.color != "undefined")
                        color2 = costum.css.loader.ring2.color;
                }

            }
            themeObj.blockUi.processingMsg =
                '<div class="lds-css ng-scope">' +
                '<div style="width:100%;height:100%" class="lds-dual-ring">' +
                '<img src="' + logoLoader + '" class="loadingPageImg" height=80>' +
                '<div style="border-color: transparent ' + color2 + ' transparent ' + color2 + ';"></div>' +
                '<div style="border-color: transparent ' + color1 + ' transparent ' + color1 + ';"></div>' +
                '</div>' +
                '</div>';
            themeObj.blockUi.errorMsg =
                '<img src="' + logoLoader + '" class="logo-menutop" height=80>' +
                '<i class="fa fa-times"></i><br>' +
                '<span class="col-md-12 text-center font-blackoutM text-left">' +
                '<span class="letter letter-red font-blackoutT" style="font-size:40px;">404</span>' +
                '</span>' +

                '<h4 style="font-weight:300" class=" text-dark padding-10">' +
                'Oups ! Une erreur s\'est produite' +
                '</h4>' +
                '<span style="font-weight:300" class=" text-dark">' +
                'Vous allez être redirigé vers la page d\'accueil' +
                '</span>';
        },
        processingMsg: "",
        errorMsg: "",
        /* ****************
        Generic ajax panel loading process 
        loads any REST Url endpoint returning HTML into the content section
        also switches the global Title and Icon
        **************/
        show: function () {
            msg = '<h4 style="font-weight:300" class=" text-dark padding-10">' +
                '<i class="fa fa-spin fa-circle-o-notch"></i><br>' + trad.currentlyloading + '...' +
                '</h4>';

            if (jsonHelper.notNull("themeObj.blockUi.processingMsg"))
                msg = themeObj.blockUi.processingMsg;
            $.blockUI({ message: msg });
        }
    },
    dynForm: {
        onLoadPanel: function (elementObj) {
            mylog.log("elementObj", elementObj);
            var typeName = (typeof currentKFormType != "undefined" && currentKFormType != null && currentKFormType != "") ?
                trad["add" + currentKFormType] : elementObj.dynForm.jsonSchema.title;
            var typeIcon = (typeof currentKFormType != "undefined" && currentKFormType != null && currentKFormType != "") ? dyFInputs.get(currentKFormType).icon : elementObj.dynForm.jsonSchema.icon;

            $("#ajax-modal-modal-title").html(
                "<i class='fa fa-" + typeIcon + "'></i> " + typeName);

            $("#ajax-modal .modal-header").removeClass("bg-dark bg-red bg-purple bg-green bg-green-poi bg-orange bg-turq bg-yellow bg-url");
            $("#ajax-modal .infocustom p").removeClass("text-dark text-red text-purple text-green text-green-poi text-orange text-turq text-yellow text-url");

            if (typeof currentKFormType != "undefined" && typeObj[currentKFormType] && typeObj[currentKFormType].color) {
                $("#ajax-modal .modal-header").addClass("bg-" + typeObj[currentKFormType].color);
                $("#ajax-modal .infocustom p").addClass("text-" + typeObj[currentKFormType].color);
            }


            $(".locationBtn").html("<i class='fa fa-home'></i> " + tradDynForm.mainLocality);
            $(".locationBtn").addClass("letter-red bold");
            $("#btn-submit-form").removeClass("text-azure").addClass("letter-green");
            /***************
            **TODO BOUBOULE QUESTION ---- WHYYYYYYY THAT ?????
            if(typeof currentKFormType != "undefined")
                $("#ajaxFormModal #type").val(currentKFormType); **/
        }
    },
    initMyScopes: function () {
        if (notNull(localStorage) && notNull(localStorage.myScopes))
            myScopes = JSON.parse(localStorage.getItem("myScopes"));
        if (notNull(myScopes) && myScopes.userId == userId) {
            myScopes.open = {};
            myScopes.countActive = 0;
            myScopes.search = {};
            myScopes.openNews = {};
            if (myScopes.multiscopes == null)
                myScopes.multiscopes = {};
            //console.log("init scope", myScopes);
        } else {
            myScopes = {
                type: "open",
                typeNews: "open",
                userId: userId,
                open: {},
                openNews: {},
                countActive: 0,
                search: {},
                communexion: themeObjCommunexion,
                multiscopes: themeObjMultiscopes
            };

            if (myScopes.communexion != false)
                myScopes.communexion = scopeObject(myScopes.communexion);
            else
                myScopes.communexion = {};
            //console.log("init scope", myScopes);
            localStorage.setItem("myScopes", JSON.stringify(myScopes));
        }
        //return myScopes;
    }
};