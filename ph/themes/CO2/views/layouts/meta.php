<?php 
$metaTitle = (isset($this->module->pageTitle)) ? $this->module->pageTitle : Yii::app()->session['paramsConfig']["metaTitle"]; 
$metaDesc = (isset($this->module->description)) ? $this->module->description : @Yii::app()->session['paramsConfig']["metaDesc"];  
$metaAuthor = (isset($this->module->author)) ? $this->module->author : @Yii::app()->session['paramsConfig']["metaAuthor"];  
$metaImg = (isset($this->module->image)) ? Yii::app()->getRequest()->getBaseUrl(true).$this->module->image : "https://co.viequotidienne.re/"."/themes/CO2".@Yii::app()->session['paramsConfig']["metaImg"]; 
$metaRelCanoncial=(isset($this->module->relCanonical)) ? $this->module->relCanonical : "https://www.communecter.org";
$keywords = ""; 
if(isset($this->module->keywords))
    $keywords = $this->module->keywords; 
else if(isset($this->keywords) )
    $keywords = $this->keywords; 
if(isset($this->module->favicon) )
    $favicon = $this->module->favicon;   
else  
    $favicon =(isset($this->module->assetsUrl)) ? $this->module->assetsUrl."/images/favicon.ico" : "/images/favicon.ico"; 
?>
<html lang="en" class="no-js">   

    <head>
        <title><?php echo $metaTitle;?></title> 
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <meta name="title" content="<?php echo $metaTitle; ?>"> 
        <meta name="description" content="<?php echo $metaDesc; ?>"> 
        <meta name="author" content="<?php echo $metaAuthor; ?>"> 
        <meta property="og:image" content="<?php echo $metaImg; ?>"/> 
        <meta property="og:description" content="<?php echo $metaDesc; ?>"/> 
        <meta property="og:title" content="<?php echo $metaTitle; ?>"/>
        <meta property="og:url" content="<?php echo Yii::app()->getRequest()->getBaseUrl(true).$metaRelCanoncial; ?>"/>
        <meta property="og:type" content="website"/> 
        <meta property="og:locale" content="fr_FR"/>
        <meta property="og:site_name" content="<?php echo Yii::app()->getRequest()->getBaseUrl(true); ?>"/>
        
        <meta name="keywords" lang="<?php echo Yii::app()->language; ?>" content="<?php echo CHtml::encode($keywords); ?>">
        <link rel="canonical" href="<?php echo $metaRelCanoncial ?>" />
 
    </head>
    <body onload="window.location.href='<?php echo $this->module->share; ?>'">

    </body>
</html>