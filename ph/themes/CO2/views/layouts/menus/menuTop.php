<?php //$themeParams=Yii::app()->session['paramsConfig']; ?>
<style>
    
</style>
<!-- Navigation -->
<nav id="mainNav" class="navbar-default col-xs-12 navbar-custom menuTop <?php echo @$menuApp ?>">
<?php 

if(@$themeParams["header"]["menuTop"] && is_string($themeParams["header"]["menuTop"])){
    $this->renderPartial( $themeParams["header"]["menuTop"]  ); 
}else{ ?>
    <div>
    <!-- //////////////// CONSTRUCTION OF LEFT NAV //////////////////// --> 
    <?php 
    if(isset($themeParams["header"]["menuTop"]) && isset($themeParams["header"]["menuTop"]["navLeft"])){  ?>
    <!-- <div class="navLeft margin-left-10"> -->
    <div class="navLeft">
        <?php


        // var_dump($themeParams["header"]["menuTop"]["navLeft"]);
        // var_dump(Yii::app()->session["costum"]["htmlConstruct"]["header"]["menuTop"]["navLeft"]);
        foreach(@$themeParams["header"]["menuTop"]["navLeft"] as $key => $value){
        // LOGO HTML NAV BAR 
        if($key=="logo"){ ?>
        <a href="#welcome" class="btn btn-link menu-btn-back-category pull-left no-padding lbh menu-btn-top" >
            <?php 
            $logo = (@Yii::app()->session['costum']["logo"]) ? Yii::app()->session['costum']["logo"] : Yii::app()->theme->baseUrl.$value["url"];
            $logoMin = (@Yii::app()->session['costum']["logoMin"]) ? Yii::app()->session['costum']["logoMin"] : $logo;
            $height = (@$value["height"]) ? $value["height"] : 40;
            // var_dump(Yii::app()->session['costum']);
            // var_dump($logo);
            // var_dump($logoMin);
            ?>
            <img src="<?php echo $logo;?>" class="logo-menutop pull-left hidden-xs" height="<?php echo $height ?>">
            <img src="<?php echo $logoMin;?>" class="logo-menutop pull-left visible-xs" height="40">
        </a>
        <?php } else if($key=="app"){
            $class = ( !empty($value["class"]) ?$value["class"] : "pull-left hidden-xs menu-app-top" );
            ?>
            <!-- <div class="col-xs-7 pull-left menu-app-top">
                <div class="hidden-xs img-menuApp pull-right"> -->
            <div class="<?php echo $class;  ?>">
                <?php echo ButtonCtk::app($value);  ?>
            </div>

        <?php

        // END LOGO HTML NAV BAR

        }else if($key=="searchBar"){ 
           // $value["dropdownResult"]=$dropdownResult;
            echo ButtonCtk::searchBar($value);    
        // END INPUT SEARCH BAR IN NAV -->
        }else if($key=="useFilter"){   
            if(isset($value["scopeFilter"]) && $value["scopeFilter"]){ ?> 
                <button class="btn hidden-xs pull-left menu-btn-scope-filter text-red elipsis margin-right-10 navbar-item-left"
                        data-type="<?php echo @$type; ?>" style="display: none;">
                        <i class="fa fa-map-marker"></i> <span class="header-label-scope"><?php echo Yii::t("common","where ?") ?></span>
                </button>
            <?php } 
            if(isset($value["showFilter"]) && $value["showFilter"]){ ?>
                <button class="btn btn-show-filters pull-left hidden-xs navbar-item-left" style="display: none;"> 
                    <i class="fa fa-filter visible-sm pull-left" style="font-size:18px;"></i>
                        <span class="hidden-sm"><?php echo Yii::t("common", "Filters") ?></span> <span class="topbar-badge badge animated bounceIn badge-warning"></span> 
                    <i class="fa fa-angle-down"></i>
                </button>
            <?php }
            
        }
    } ?>
        <div class="dropdown dropdownApps text-center pull-left" id="dropdown-apps">
            <button class="dropdown-toggle menu-button btn-menu menu-btn-top text-dark" type="button" id="dropdownApps" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-placement="bottom" title="Applications" alt="Applications">
                  <i class="fa fa-th"></i>
            </button>
        </div>
    </div> 
    <?php }
    ///////////////////////////////////////////////////////////////
    //////////////// END CONSTRUCT OF LEFT NAV ////////////////////
    ///////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////
    //////////////// CONSTRUCT OF RIGHT NAV ////////////////////
    ///////////////////////////////////////////////////////////////
    //var_dump($menuRight);
    if(@$themeParams["header"]["menuTop"] && @$themeParams["header"]["menuTop"]["navRight"]){ 
        $menuRight=(@Yii::app()->session['userId']) ? $themeParams["header"]["menuTop"]["navRight"]["connected"] : $themeParams["header"]["menuTop"]["navRight"]["disconnected"]; ?> 
    <div id="navbar" class="navbar-collapse pull-right navbar-right margin-right-15">
        <?php 
        // var_dump($menuRight);exit;
        foreach($menuRight as $key => $value){
            if($key=="map"){ ?>
                <span class="btn-show-map pull-right"
                        title="<?php echo Yii::t("common", "Show the map"); ?>"
                        alt="<?php echo Yii::t("common", "Show the map"); ?>"
                        >
                    <i class="fa fa-map-marker"></i>
                </span>
            <?php } 
            if($key=="languages"){ ?>
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle btn btn-default btn-language padding-5" style="padding-top: 7px !important" data-toggle="dropdown" role="button">
                        <img src="<?php echo Yii::app()->getRequest()->getBaseUrl(true); ?>/images/flags/<?php echo Yii::app()->language ?>.png" width="22"/> <span class="caret"></span></a>
                        <ul class="dropdown-menu arrow_box dropdown-languages-nouser" role="menu" style="">
                            <?php foreach($themeParams["languages"] as $lang => $label){ ?>
                                    <li><a href="javascript:;" onclick="coInterface.setLanguage('<?php echo $lang ?>')"><img src="<?php echo Yii::app()->getRequest()->getBaseUrl(true); ?>/images/flags/<?php echo $lang ?>.png" width="25"/> <?php echo Yii::t("common",$label) ?></a></li>
                                <?php } ?>
                        </ul>
                    </li>
                </ul> 
            <?php }
            if($key=="login"){ ?>
                <button class="letter-green font-montserrat btn-menu-connect margin-left-10 margin-right-10 menu-btn-top" 
                        data-toggle="modal" data-target="<?php echo $value["dataTarget"] ?>" style="font-size: 17px;">
                        <i class="fa fa-<?php echo $value["icon"] ?>"></i> 
                        <span class="hidden-xs"><small style="width:70%;"><?php echo Yii::t("login", $value["label"]) ?></small></span>
                </button>
            <?php }
            if($key=="dropdownUser"){ ?>
                <button class="btn-show-mainmenu btn btn-link btn-menu-tooltips pull-right menu-btn-top" 
                        data-toggle="tooltip" data-placement="top" title="<?php echo Yii::t("common","Menu") ?>">
                    <i class="fa fa-bars tooltips" ></i>
                    <span class="tooltips-top-menu-btn"><?php echo Yii::t("common", "Menu"); ?></span>
                </button>
                <div class="dropdown pull-right" id="dropdown-user">
                    <div class="dropdown-main-menu">
                        <ul class="dropdown-menu arrow_box">
                        <?php 
                        $nbLi=count($value);
                        $i=0;
                        foreach($value as $k => $v){ 
                            $i++;
                            $label=@$v["label"];
                            $show=true;
                            $href=($k=="logout") ? Yii::app()->createUrl($v["href"]) : $v["href"];
                            if(strpos($href, "{slug}") !== false && isset(Yii::app()->session["user"]) && isset(Yii::app()->session["user"]["slug"]))
                                $href=str_replace("{slug}", "@".Yii::app()->session["user"]["slug"], $href);
                            $blank=(@$blank) ? "target='_blank'" : "";
                            if($k=="admin"){ 
                                if(Yii::app()->session["userIsAdmin"] || 
                                   Yii::app()->session[ "userIsAdminPublic" ] || 
                                   Yii::app()->session["isCostumAdmin"] ||
                                    (isset(Yii::app()->session["costum"]) && isset($v["onlyMember"]) && Authorisation::isElementMember(Yii::app()->session["costum"]["contextId"],Yii::app()->session["costum"]["contextType"], Yii::app()->session["userId"]))){
                                    $show=true;
                                    $label=(Yii::app()->session["userIsAdmin"]) ? Yii::t("common", "Admin") : Yii::t("common", "Admin public");

                                    if(Yii::app()->session["userIsAdmin"])
                                        echo '<li class="text-center">Super Admin</li>';
                                    else if(Yii::app()->session[ "userIsAdminPublic" ])
                                        echo '<li class="text-center">Admin Public</li>';
                                    else if( Yii::app()->session["isCostumAdmin"])
                                        echo '<li class="text-center">Costum Admin</li>';
                                }else
                                    $show=false;
                            }
                            if($show){
                        ?>
                            <li class="<?php echo @$v["liClass"] ?>">
                                <a href="<?php echo @$href ?>" class="bg-white <?php echo @$v["aClass"] ?>" <?php echo $blank ?>>
                                    <i class="fa fa-<?php echo @$v["icon"] ?>"></i> <?php echo Yii::t("common", $label) ; ?>
                                </a>
                                <?php if($k=="languages"){ ?>
                                    <ul class="dropdown-menu">
                                    <?php foreach($themeParams["languages"] as $lang => $label){ ?>
                                        <li><a href="javascript:;" onclick="coInterface.setLanguage('<?php echo $lang ?>')"><img src="<?php echo Yii::app()->getRequest()->getBaseUrl(true); ?>/images/flags/<?php echo $lang ?>.png"/><span class="hidden-xs"><?php echo Yii::t("common",$label) ?></span></a></li>
                                    <?php } ?>
                                    </ul>
                                <?php } ?>
                            </li>
                            <?php if($i<$nbLi){ ?>
                            <li role="separator" class="divider"></li>
                            <?php }
                            } 
                        } ?>
                        </ul>
                    </div>
                </div>
            <?php }

            if($key=="app" && !empty($value)){ ?>
                <div class="pull-left menu-app-top hidden-xs">
                    <?php echo ButtonCtk::app($value);  ?>
                </div>
            <?php
            // END LOGO HTML NAV BAR
            }
            if($key=="userProfil"){
                  $profilThumbImageUrl = Element::getImgProfil($me, "profilThumbImageUrl", $this->module->getParentAssetsUrl()); ?> 
                <a  href="#page.type.citoyens.id.<?php echo Yii::app()->session['userId']; ?>"
                        class="menu-name-profil lbh text-dark pull-right shadow2 btn-menu-tooltips" 
                        data-toggle="dropdown">

                        <?php if(@$value["name"] && !empty($value["name"])){ ?> 
                            <small class="hidden-xs hidden-sm margin-left-10" id="menu-name-profil">
                                <?php echo @$me["name"] ? $me["name"] : @$me["username"]; ?>
                            </small> 
                        <?php } 
                        if(@$value["img"]){ ?>
                        <img class="img-circle" id="menu-thumb-profil" 
                             width="40" height="40" src="<?php echo $profilThumbImageUrl; ?>" alt="image" >
                        <?php } ?>
                    <span class="tooltips-top-menu-btn"><?php echo Yii::t("common", "My page"); ?></span>
                </a>
            <?php } 
            if($key=="networkFloop"){ ?>
                <button class="menu-button btn-menu btn-link btn-open-floopdrawer text-dark pull-right hidden-xs btn-menu-tooltips menu-btn-top" 
                      data-toggle="tooltip" data-placement="bottom" title="<?php echo Yii::t("common","My network") ?>" 
                      alt="<?php echo Yii::t("common","My network") ?>">
                <?php if(isset(Yii::app()->session["costum"]["css"]["menuRight"]["img"]["networkFloop"])) {
                    ?>
                    <img width="45px" height="45px" src="<?php echo Yii::app()->getModule("costum")->assetsUrl.Yii::app()->session["costum"]["css"]["menuRight"]["img"]["networkFloop"]; ?>">
                <?php } else { 
                ?>
                    <i class="fa fa-users"></i> <?php } ?>
                    <span class="tooltips-top-menu-btn"><?php echo Yii::t("common", "My network"); ?></span>
                </button>
            <?php }
            if($key=="notifications"){ 
                $countNotifElement = ActivityStream::countUnseenNotifications(Yii::app()->session["userId"], Person::COLLECTION, Yii::app()->session["userId"]); ?>
                <button class="menu-button btn-menu btn-menu-notif text-dark pull-right btn-menu-tooltips menu-btn-top" 
                      data-toggle="tooltip" data-placement="bottom" title="<?php echo Yii::t("common","Notifications") ?>" alt="<?php echo Yii::t("common","Notifications") ?>">
                <?php if(isset(Yii::app()->session["costum"]["css"]["menuRight"]["img"]["notifications"])) {
                    ?>
                    <img width="45px" height="45px" src="<?php echo Yii::app()->getModule("costum")->assetsUrl.Yii::app()->session["costum"]["css"]["menuRight"]["img"]["notifications"]; ?>">
                <?php } else { 
                ?>
                  <i class="fa fa-bell"></i> <?php } ?>
                  <span class="notifications-count topbar-badge badge animated bounceIn 
                          <?php if(!@$countNotifElement || (@$countNotifElement && $countNotifElement=="0")) 
                          echo 'badge-transparent hide'; else echo 'badge-success'; ?>">
                        <?php echo @$countNotifElement ?>
                    </span>
                    <span class="tooltips-top-menu-btn"><?php echo Yii::t("common", "My notifications"); ?></span>
                </button>
            <?php }
            if($key=="dda"){
                if(@$me && @$me["links"] && (@$me["links"]["memberOf"] || @$me["links"]["contributors"])){ ?>
                    <button class="menu-button btn-menu btn-dashboard-dda text-dark pull-right hidden-xs menu-btn-top" 
                          data-toggle="tooltip" data-placement="bottom" title="<?php echo Yii::t("common","Cooperation") ?>" 
                          alt="<?php echo Yii::t("common","Cooperation") ?>">
                <?php if(isset(Yii::app()->session["costum"]["css"]["menuRight"]["img"]["dda"])) {
                    ?>
                    <img width="45px" height="45px" src="<?php echo Yii::app()->getModule("costum")->assetsUrl.Yii::app()->session["costum"]["css"]["menuRight"]["img"]["dda"]; ?>">
                <?php } else { 
                ?>
                      <i class="fa fa-inbox"></i> <?php } ?>
                      <span class="coopNotifs topbar-badge badge animated bounceIn badge-warning"></span>
                    </button>
            <?php } 
            } 
            if($key=="chat"){ ?>                  
                <button class="menu-button btn-menu btn-menu-chat text-dark pull-right hidden-xs btn-menu-tooltips menu-btn-top" 
                      onClick='rcObj.loadChat("","citoyens", true, true)' data-toggle="tooltip" data-placement="bottom" 
                      title="<?php echo Yii::t("common","Messaging") ?>" alt="<?php echo Yii::t("common","Messaging") ?>">
                <?php if(isset(Yii::app()->session["costum"]["css"]["menuRight"]["img"]["chat"])) {
                    ?>
                    <img width="45px" height="45px" src="<?php echo Yii::app()->getModule("costum")->assetsUrl.Yii::app()->session["costum"]["css"]["menuRight"]["img"]["chat"]; ?>">
                <?php } else { 
                ?>
                  <i class="fa fa-comments"></i>
                <?php } ?>
                  <span class="chatNotifs topbar-badge badge animated bounceIn badge-warning"></span>
                  <span class="tooltips-top-menu-btn"><?php echo Yii::t("common", "My chat"); ?></span>
                </button>
            <?php }
            if($key=="wekan"){ ?>                  
                <a href="https://wekan.communecter.org/" target="_blank" class="menu-button btn-menu btn-menu-chat text-dark pull-right hidden-xs btn-menu-tooltips menu-btn-top padding-15" 
                    data-toggle="tooltip" data-placement="bottom" 
                    title="<?php echo Yii::t("common","Wekan") ?>" 
                    alt="<?php echo Yii::t("common","Wekan") ?>">
                  <i class="fa fa-tasks"></i>
                  <span class="chatNotifs topbar-badge badge animated bounceIn badge-warning"></span>
                  <span class="tooltips-top-menu-btn"><?php echo Yii::t("common", "Wekan"); ?></span>
                </a>
            <?php }
            if($key=="home"){ ?>
                <a href="#home" class="lbh menu-button btn-menu btn-menu-home text-dark pull-right btn-menu-tooltips menu-btn-top" 
                       data-toggle="tooltip" data-placement="bottom" 
                      title="<?php echo Yii::t("common","Home") ?>" alt="<?php echo Yii::t("common","Home") ?>" style="width: inherit !important;text-transform: capitalize;">
                <?php if(isset(Yii::app()->session["costum"]["css"]["menuRight"]["img"]["home"])) {
                    ?>
                    <img width="45px" height="45px" src="<?php echo Yii::app()->getModule("costum")->assetsUrl.Yii::app()->session["costum"]["css"]["menuRight"]["img"]["home"]; ?>">
                <?php } else { 
                ?>
                  <i class="fa fa-home"></i> <span class="hidden-xs hidden-sm" style="font-size: 16px;"><?php echo Yii::t("common","Home") ?></span>
                  <span class="tooltips-top-menu-btn"><?php echo Yii::t("common", "My home"); ?></span> <?php } ?>
                </a>
            <?php }
            
        } ?>
        </div>
    <?php }

        if(!empty($themeParams["header"]["menuTop"]["navRight"]["searchBar"])){ 
            $value = $themeParams["header"]["menuTop"]["navRight"]["searchBar"];
            echo ButtonCtk::searchBar($value);    
        // END INPUT SEARCH BAR IN NAV -->
        } 
        if(isset($key) && $key=="nameMenuTop"){  ?>
        <div class="nameMenuTop col-xs-12 col-sm-12">
            <?php echo (@Yii::app()->session["costum"]["nameMenuTop"]["message"] ? Yii::app()->session["costum"]["nameMenuTop"]["message"]: ""); ?>
        </div>
        <?php } ?>
    </div>
    <?php 
    }
    // Pour faire afficher un message sous le search bar pour costum 
    if(@Yii::app()->session["costum"]["htmlConstruct"]["header"]["menuTop"]["navLeft"]["nameMenuTop"] && @Yii::app()->session['userId']) {
        $top = (Yii::app()->session["costum"]["htmlConstruct"]["header"]["menuTop"]["navLeft"]["nameMenuTop"]["top"] ? Yii::app()->session["costum"]["htmlConstruct"]["header"]["menuTop"]["navLeft"]["nameMenuTop"]["top"] : 0);
        ?>
        <div class="nameMenuTop col-xs-12 col-sm-12" style="margin-top : -<?php echo $top; ?>%; font-family: 'customFont'">
        <?php echo Yii::app()->session["costum"]["htmlConstruct"]["header"]["menuTop"]["navLeft"]["nameMenuTop"]["name"]; ?>
        </div>
    <?php } ?>

    
</nav>


<!-- DROPDOWNS OF MENUTOP -->
<div class="dropdown dropdownApps-menuTop" aria-labelledby="dropdownApps">
        <div class="dropdown-menu arrow_box no-padding">
            <?php   
            if(@$themeParams){
                foreach (@$themeParams["pages"] as $key => $value) {
                    if(@$value["inMenu"]==true){ 
                        if((!isset($value["onlyAdmin"]) || Authorisation::isInterfaceAdmin()) 
                            && (!isset($value["onlyMember"]) || Authorisation::isInterfaceAdmin() || (isset(Yii::app()->session["costum"]["isMember"]) && !empty(Yii::app()->session["costum"]["isMember"])))){

                        if(!empty($value["urlExtern"])){ ?>
                            <a class="dropdown-item padding-5 text-center col-xs-12 " href="<?php echo $value["urlExtern"]; ?>" target="_blanc" data-toggle="tooltip" data-placement="bottom" >
                        <?php
                        } else { ?>
                            <a class="dropdown-item padding-5 text-center col-xs-12 lbh-menu-app" href="javascript:;" data-hash="<?php echo $key; ?>" data-toggle="tooltip" data-placement="bottom" >
                        <?php
                        }
                        ?>
                        
                            <?php if (!empty($value["img"])){
                                echo '<img class="imgMenuSmall" style="width:5vw;" src="'.Yii::app()->getModule("costum")->assetsUrl.$value["img"].'">';
                            }elseif(!empty($value["icon"])){
                                echo '<i class="fa fa-'.$value["icon"].' "></i>';
                            }?> 
                            <span class="<?php echo str_replace("#","",$key); ?>ModSpan"><?php echo Yii::t("common", @$value["subdomainName"]); 
                            if (!empty($value["nameMenuTop"])) {
                                echo $value["nameMenuTop"];
                            }
                            ?></span></a>
                <?php } 
                    } 
                }
            } ?>
        </div>
</div>
 <div class="dropdown pull-right" id="dropdown-dda">
    <div class="dropdown-main-menu">
        <ul class="dropdown-menu arrow_box menuCoop" id="list-dashboard-dda">
            
        </ul>
    </div>
</div>
<?php 
$this->renderPartial($layoutPath.'loginRegister'/*, array("subdomain" => @$subdomain)*/); 

$this->renderPartial($layoutPath.'formCreateElement'); ?>

